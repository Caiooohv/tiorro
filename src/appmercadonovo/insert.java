/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import appmercadonovo.conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class insert {
    public void insProduto(String nome, String codigo, String preco, String estoque){
        try {
            Connection c = conexao.getConnection();
            PreparedStatement ps = c.prepareStatement("insert into produto(id, nome, valor, estoque) values(?,?,?,?)");
            ps.setString(1, codigo);
            ps.setString(2, nome);
            ps.setString(3, preco);
            ps.setString(4, estoque);
            
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(insert.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void inserir2(String cpf,String nome, String telefone){
        try {
            Connection c = conexao.getConnection();
            PreparedStatement ps = c.prepareStatement("insert into sc_natacao.professor(cpf, nome, telefone) values(?,?,?)");
            ps.setString(1, cpf);
            ps.setString(2, nome);
            ps.setString(3, telefone);
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(insert.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
