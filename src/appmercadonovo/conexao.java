/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appmercadonovo;

import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JOptionPane;

/**
 *
 * @author Elias Magalhães
 */
public class conexao {
    private static final String STR_CONEXAO = "jdbc:mysql://localhost/pingo?serverTimezone=UTC";
    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    //private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String USUARIO = "turi";
    private static final String SENHA = "turi";
    private static Connection connection;
   
    public static Connection getConnection(){
        if(connection == null){
            new conexao();
        }
        return connection;
    }
   
    private conexao(){
        try{
            Class.forName(DRIVER).newInstance();
            this.connection = DriverManager.getConnection(STR_CONEXAO,USUARIO,SENHA);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null,
                    "Conexão não estabelecida\n "+ex.getMessage(),
                    "Erro na Conexão com o SGDB",JOptionPane.INFORMATION_MESSAGE);
           
        }
    }
   
    public void closeConnection() throws Exception{
        connection.close();
    }
}