/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package appmercadonovo;

import java.util.Objects;
import javax.swing.JOptionPane;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class Gerente {
    private String User;
    String Pass;
    public Gerente(String user, String pass){
        this.User = user;
        this.Pass = pass;
    }
    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }
    public String getUser() {
        return User;
    }
    public String getPass(){
        return this.Pass;
    }
    @Override
    
    public String toString() {
        return "User: "+this.User+ "   Pass: "+this.Pass;
        
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Gerente other = (Gerente) obj;
        if (!Objects.equals(this.User, other.User)) {
            return false;
        }
        if (!Objects.equals(this.Pass, other.Pass)) {
            return false;
        }
        return true;
    }
}
