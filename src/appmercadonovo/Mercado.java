/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appmercadonovo;

/**
 *
 * @author DjHvBeat
 */
public class Mercado {
    private String nome;
    private double saldo;
    public void setNome(String nome){
        this.nome = nome;
    }
    public void setSaldo(double saldo){
        this.saldo = saldo;
    }
    public double compra(double preço, double pagamento){
        double troco = pagamento - preço;
        return troco;
    }
    public String getNome(){
        return this.nome;
    }
    public double getSaldo(){
        return this.saldo;
    }
}
